<?php
require_once '../lib/DataSourceResult.php';
require_once '../lib/Kendo/Autoload.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    header('Content-Type: application/json');

    $request = json_decode(file_get_contents('php://input'));
	
	

    $result = new DataSourceResult('');
	
	//var_dump($result);

    //echo json_encode($result->read('Customers', array('CustomerID', 'ContactName', 'ContactTitle', 'CompanyName', 'Country'), $request));

	echo json_encode($result->read('TB_PAT', array('KD_PAT', 'KET'), $request));

    exit;
}

require_once '../include/header.php';

$transport = new \Kendo\Data\DataSourceTransport();

$read = new \Kendo\Data\DataSourceTransportRead();

$read->url('index.php')
     ->contentType('application/json')
     ->type('POST');

$transport ->read($read)
          ->parameterMap('function(data) {
              return kendo.stringify(data);
          }');

$model = new \Kendo\Data\DataSourceSchemaModel();

$kdpatField = new \Kendo\Data\DataSourceSchemaModelField('KD_PAT');
$kdpatField->type('string');

$ketField = new \Kendo\Data\DataSourceSchemaModelField('KET');
$ketField->type('string');

$model->addField($kdpatField)
      ->addField($ketField);

$schema = new \Kendo\Data\DataSourceSchema();
$schema->data('data')
       ->errors('errors')
       ->groups('groups')
       ->model($model)
       ->total('total');

$dataSource = new \Kendo\Data\DataSource();

$dataSource->transport($transport)
           ->pageSize(10)
           ->serverPaging(true)
           ->serverSorting(true)
           ->serverGrouping(true)
           ->schema($schema);

$grid = new \Kendo\UI\Grid('grid');

$kdpat = new \Kendo\UI\GridColumn();
$kdpat->field('KD_PAT')
            ->title('Kode PAT')
            ->width(240);

$ket = new \Kendo\UI\GridColumn();
$ket->field('KET')->title('UNIT KERJA');

$pageable = new Kendo\UI\GridPageable();
$pageable->refresh(true)
      ->pageSizes(true)
      ->buttonCount(5);

$grid->addColumn($kdpat, $ket)
     ->dataSource($dataSource)
     ->sortable(true)
     ->groupable(true)
     ->pageable($pageable)
     ->attr('style', 'height:550px');

echo $grid->render();
?>

<style type="text/css">
   
</style>

<?php require_once '../include/footer.php'; ?>
