<?php

class Posisi_kredit extends CI_Controller {
    
    public function __construct() {
        parent ::__construct();
        
        //load model
        $this->load->model('model_app');
    }

    public function index() {
        
        $this->load->view('v_posisi_kredit');
    }

    public function getKodeBank()
    {
        echo json_encode($this->model_app->getKodeBank());
    }

    public function getAllHutang()
    {
        $data   = $this->input->post('data');
        echo json_encode($this->model_app->getHutang($data));
    }

    public function getRincian()
    {
        $data = $this->input->post('data');
        echo json_encode($this->model_app->getSelectedBank($data));
        // echo $data;
    }

    public function addHutang()
    {
        // $no                 = $this->input->post('NO');
        $kode_bank          = $this->input->post('KD_BANK');
        $supplier           = $this->input->post('SUPPLIER');
        $rupiah             = $this->input->post('RUPIAH');
        $tgl_jatuh_tempo    = $this->input->post('TGL_JATUH_TEMPO');
        $ket                = $this->input->post('KET');
        $bunga              = $this->input->post('BUNGA');
        $tgl_penarikan      = $this->input->post('TANGGAL_PENARIKAN');

        $this->model_app->addHutang($kode_bank, $supplier, $rupiah, $tgl_jatuh_tempo, $ket, $bunga, $tgl_penarikan);
    }
}