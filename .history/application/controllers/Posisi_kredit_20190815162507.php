<?php

class Posisi_kredit extends CI_Controller {
    
    public function __construct() {
        parent ::__construct();
        
        //load model
        $this->load->model('model_app');
    }

    public function index() {
        $data['stat'] = '';
        $this->load->view('v_posisi_kredit', $data);
        // $this->load->view('v_posisi_kredit');
    }

    public function getKodeBank()
    {
        echo json_encode($this->model_app->getKodeBank());
    }

    public function getAllHutang()
    {
        $data   = $this->input->post('data');
        echo json_encode($this->model_app->getHutang($data));
    }

    public function getRincian()
    {
        $data = $this->input->post('data');
        echo json_encode($this->model_app->getSelectedBank($data));
        // echo $data;
    }

    public function addHutang()
    {
        // $no                 = $this->input->post('NO');
        $kode_bank          = $this->input->post('kdbank');
        $supplier           = $this->input->post('Supplier');
        $rupiah             = $this->input->post('Rupiah');
        $tgl_jatuh_tempo    = $this->input->post('tgljatuhtempo');
        $ket                = $this->input->post('Keterangan');
        $bunga              = $this->input->post('Bunga');
        $tgl_penarikan      = $this->input->post('tglpenarikan');

        $this->model_app->addHutang($kode_bank, $supplier, $rupiah, $tgl_jatuh_tempo, $ket, $bunga, $tgl_penarikan);

        // $data['stat'] = $kode_bank;
        // $this->load->view('v_posisi_kredit', $data);
        // $this->load->view('v_posisi_kredit');
    }
}