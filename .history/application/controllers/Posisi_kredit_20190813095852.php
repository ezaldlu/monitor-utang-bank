<?php

class GcgController extends CI_Controller {

    public $varQuestionId = "question id";
    
    public function __construct() {
        parent ::__construct();
        
        //load model
        $this->load->model('GcgModel');
    }

    public function index() {
        $data['stat'] = '3';
        $this->load->view('gcg', $data);
    }
}