<?php

class Posisi_kredit extends CI_Controller {
    
    public function __construct() {
        parent ::__construct();
        
        //load model
        $this->load->model('model_app');
    }

    public function index() {
        
        $this->load->view('v_posisi_kredit');
    }

    public function getKodeBank()
    {
        echo json_encode($this->model_app->getKodeBank());
    }

    public function getAllHutang()
    {
        $data   = $this->input->post('data');
        echo json_encode($this->model_app->getHutang($data));
    }

    public function addRincian()
    {
        $tanggal   = $this->input->post('TANGGAL');
        $kode_bank = $this->input->post('KODE');


    }
}