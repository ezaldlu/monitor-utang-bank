<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Model_App extends CI_Model
{
    // public function logged_in()
    // {
        
    // }

    // public function logged_out()
    // {

    // }
    public function getHutang($data)
    {
        $query = $this->db->query("SELECT * FROM TB_HUTANG WHERE KD_BANK = '".$data."'")->result();
        if ($query) {
            # code...
            return $query;
          } else {
            return false;
          }
    }

    public function getSelectedBank($data)
    {
        $query = $this->db->query("SELECT * FROM TB_HUTANG_RINCIAN
                                  LEFT JOIN TB_HUTANG ON TB_HUTANG.NO = TB_HUTANG_RINCIAN.NO
                                  WHERE TB_HUTANG_RINCIAN.NO = ".$data."")->result();
        if ($query) {
          # code...
          return $query;
        } else {
          return false;
        }
        
    }
    
    public function getKodeBank()
    {
        $query = $this->db->query("SELECT KD_BANK AS KODE, KET AS KET FROM TB_BANK")->result();

        if ($query) {
            # code...
            return $query;
          } else {
            return false;
          }
  
    }

    public function addHutang($kode_bank, $supplier, $rupiah, $tgl_jatuh_tempo, $ket, $bunga, $tgl_penarikan)
    {
        $query = $this->db->query("INSERT INTO TB_HUTANG 
                                                          (
                                                                    KD_BANK, 
                                                                    SUPPLIER, 
                                                                      RUPIAH, 
                                                            TGL_JATUH_TEMPO, 
                                                                        KET, 
                                                                      BUNGA, 
                                                            TANGGAL_PENARIKAN
                                                        ) 
                                                  VALUES(
                                                        '".$kode_bank."',
                                                        '".$supplier."',
                                                        ".$rupiah.",
                                                        '".$tgl_jatuh_tempo."',
                                                        '".$ket."',
                                                        '".$bunga."',
                                                        '".$tgl_penarikan."')");

        if ($query) {
          return true;
        } else {
          return false;
        }
    }

}
