<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.material.mobile.min.css" />

    <script src="<?php echo base_url(); ?>assets/telerik-php2/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/telerik-php2/js/kendo.all.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
        

</head>
<body>
    <div class="container-fluid">
        <div class="row" style="border-bottom: 1px solid #EEEEEE">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <h1>Posisi Kredit</h1>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="padding-top: 25px;">
                <input id="BankList" style="width: 100%" />
            </div>  
        </div>
        <div class="row" style="padding-top: 20px">
            <div class="" style="padding:10px; margin: 0px">
                <button type="button" id="addButton" class="k-primary" style="padding: 10px;">Add New Data</button>
                <div id="grid"></div>
            </div>
        </div>
        <div id="createWindow" style="display: none;">
            <form id="employeeForm" data-role="validator" novalidate="novalidate">
                <ul id="fieldlist">
                    <li>
                        <label for="FirstName">First Name:</label>
                        <input type="text" class="k-textbox" name="FirstName" id="FirstName" required="required" />
                    </li> 
                    <li>
                        <label for="LastName">Last Name:</label>
                        <input type="text" class="k-textbox" name="LastName" id="LastName" required="required" />
                    </li>   
                    <li>
                        <label for="HireDate">Hire Date:</label>
                        <input type="text" data-role='datepicker' id="HireDate" name="HireDate" data-type="date" required="required"  />
                        <span data-for='HireDate' class='k-invalid-msg'></span>
                    </li> 
                    <li>
                        <label for="RetireDate">Retire Date:</label>
                        <input type="text" data-role='datepicker' id ="RetireDate" data-type="date" name="RetireDate" data-greaterdate-field="HireDate" data-greaterdate-msg='Retire date should be after Hire date' />
                        <span data-for='RetireDate' class='k-invalid-msg'></span>
                    </li> 
                    <li>
                            <button type="button" class="k-primary" data-role="button" data-click='save'>Save</button>
                    </li>       
                </ul>
            </form>
        </div>
        <div id="window" style="display: none;">
            <div id="gridRincian"></div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var addr = "<?php echo base_url(); ?>index.php/";
            var kdbank = 'hahahah';

            function onClickAddButton(e) {
                console.log("event :: click (" + $(e.event.target).closest(".k-button").attr("id") + kdbank +")" );
                createWindow.data("kendoWindow").open();
                undo.fadeOut();
            }
            $("#addButton").kendoButton({
                click: onClickAddButton
            });

            $(function () {
                var container = $("#employeeForm");
                kendo.init(container);
                container.kendoValidator({
                    rules: {
                        greaterdate: function (input) {
                            if (input.is("[data-greaterdate-msg]") && input.val() != "") {                                    
                                var date = kendo.parseDate(input.val()),
                                    otherDate = kendo.parseDate($("[name='" + input.data("greaterdateField") + "']").val());
                                return otherDate == null || otherDate.getTime() < date.getTime();
                            }

                            return true;
                        }
                    }
                });
            });

            function save(e) {
                var validator = $("#employeeForm").data("kendoValidator");
                if (validator.validate()) {
                    alert("Employee Saved");
                }
            }

            //Dropdown ---------------------------------------------------------------------------------------------//
            function onSelect(e) {
                if (e.dataItem) {
                    var dataItem = e.dataItem;
                    // console.log("event :: select (" + dataItem.KET + " : " + dataItem.KODE + ")");
                    $("#grid").data("kendoGrid").dataSource.read({data: dataItem.KODE}); 
                    // $("#grid").data("kendoGrid").dataSource.create({data: dataItem.KODE}); 
                   
                    kdbank = dataItem.KODE;
                    $.post('<?php echo base_url(); ?>index.php/Posisi_kredit/setKdBank', {
                        data: dataItem.KODE
                    });
                    // console.log(kdbank);
                }
            };

            $("#BankList").kendoDropDownList({
                dataTextField: "KET",
                dataValueField: "KODE",
                filter: "startswith",
                select: onSelect,
                dataSource: {
                    transport: {
                        read: {
                            dataType: "json",
                            url: addr + "Posisi_kredit/getKodeBank",
                        }
                    }
                }
            });
            //End ofDropdown ---------------------------------------------------------------------------------------//

            //Grid -------------------------------------------------------------------------------------------------//
            function onChange(arg) {
                var selected = $.map(this.select(), function(item) {
                    return $(item).text();
                });
                var number = (selected.join(", ")).charAt(0);
                // console.log("Selected: " + selected.length + " item(s), [" + selected.join(", ") + "]");
                
                $("#gridRincian").data("kendoGrid").dataSource.read({data: number}); 

                myWindow.data("kendoWindow").open();
                undo.fadeOut();
            }
            $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: addr + "Posisi_kredit/getAllHutang",
                            dataType: "json",
                            data: {data: "MDR006"},
                            type: "post"
                        },
                        create: {
                            url: addr + "Posisi_kredit/addHutang",
                            dataType: "json",
                            type: "post"
                        }
                    },
                    schema: {
                        model: {
                            id: "NO",
                            fields: {
                                NO: {editable: false},
                                // KD_BANK: { field: "KD_BANK",editable: false },
                                SUPPLIER: { field: "SUPPLIER" },
                                RUPIAH: { field: "RUPIAH" },
                                TGL_JATUH_TEMPO: { field: "TGL_JATUH_TEMPO", type: "date" },
                                KET: { field: "KET" },
                                BUNGA: { field: "BUNGA", type: "number" },
                                TANGGAL_PENARIKAN: { field: "TANGGAL_PENARIKAN", type: "date" },
                                CREATED_BY: { field: "CREATED_BY" },
                                CREATED_DATE: { field: "CREATED_DATE" },
                                LAST_UPDATE_BY: { field: "LAST_UPDATE_BY" },
                                LAST_UPDATE_DATE: { field: "LAST_UPDATE_DATE"}
                            }
                        }
                    },
                    pageSize: 20
                },
                selectable: "multiple",
                change: onChange,
                // toolbar: ["create"],
                editable: "popup",
                height: 550,
                groupable: true,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [{
                    field: "NO",
                    title: "No",
                    width: 40,
                }, 
                // {
                //     field: "KD_BANK",
                //     title: "Kode Bank",
                // }, 
                {
                    field: "SUPPLIER",
                    title: "Supplier",
                }, {
                    field: "RUPIAH",
                    title: "Rupiah",
                }, {
                    field: "TGL_JATUH_TEMPO",
                    title: "Tanggal Jatuh Tempo",
                }, {
                    field: "KET",
                    title: "KET",
                }, {
                    template: "#: BUNGA #%",
                    field: "BUNGA",
                    title: "Bunga",
                }, {
                    field: "TANGGAL_PENARIKAN",
                    title: "Tanggal Penarikan",
                }]
            });
            $("#gridRincian").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: addr + "Posisi_kredit/getRincian",
                            dataType: "json",
                            data: {data: 0},
                            type: "post"
                        }
                    },
                    schema: {
                        model: {
                            id: "NO_RINCIAN",
                            fields: {
                                BUNGA: { field: "BUNGA" },
                                TANGGAL_PENARIKAN: { field: "TANGGAL_PENARIKAN" },
                                TGL_JATUH_TEMPO: { field: "TANGGAL_PENARIKAN" },
                                KET: { field: "KET" },
                                CREATED_BY: { field: "CREATED_BY" },
                                CREATED_DATE: { field: "CREATED_DATE" },
                                LAST_UPDATE_BY: { field: "LAST_UPDATE_BY" },
                                LAST_UPDATE_DATE: { field: "LAST_UPDATE_DATE"}
                            }
                        }
                    },
                    pageSize: 20
                },
                height: 550,
                groupable: true,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [{
                    field: "NO_RINCIAN",
                    title: "No",
                    width: 40,
                }, {
                    field: "TGL_JATUH_TEMPO",
                    title: "Tanggal Jatuh Tempo",
                }, {
                    field: "KET",
                    title: "KET",
                }, {
                    template: "#: BUNGA #%",
                    field: "BUNGA",
                    title: "Bunga",
                }, {
                    field: "TANGGAL_PENARIKAN",
                    title: "Tanggal Penarikan",
                }, { command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }]
            });
            //End of Grid ------------------------------------------------------------------------------------------//

            //Popup ------------------------------------------------------------------------------------------------//
            
            //Window grid --------------------------------------//
            var myWindow = $("#window"),
                undo = $("#undo");

            undo.click(function() {
                myWindow.data("kendoWindow").open();
                undo.fadeOut();
            });

            function onClose() {
                undo.fadeIn();
            }

            myWindow.kendoWindow({
                width: "600px",
                title: "Rincian",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            }).data("kendoWindow").center();
            //End of Window grid ------------------------------//
            //Create Window grid ------------------------------//
            var createWindow = $("#createWindow"),
                undo = $("#undo");

            undo.click(function() {
                createWindow.data("kendoWindow").open();
                undo.fadeOut();
            });

            function onClose() {
                undo.fadeIn();
            }

            createWindow.kendoWindow({
                width: "400px",
                title: "Add New Data",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            }).data("kendoWindow").center();
            //End of Create Window grid ------------- ---------//

            //End of Popup -----------------------------------------------------------------------------------------//
        });
    </script>
</div>

<style type="text/css">
    body {
        font-family: 'Roboto';
    }
</style>
<style>
    #fieldlist {
        margin: 0;
        padding: 0;
    }

    #fieldlist li {
        list-style: none;
        padding-bottom: .7em;
        text-align: left;
    }

    #fieldlist label {
        display: block;
        padding-bottom: .3em;
        font-weight: bold;
        text-transform: uppercase;
        font-size: 12px;
        color: #444;
    }

    #fieldlist li .k-widget:not(.k-tooltip),
    #fieldlist li .k-textbox {
        margin: 0 5px 5px 0;
    }

    span.k-widget.k-tooltip-validation {
        display; inline-block;
        width: 160px;
        text-align: left;
        border: 0;
        padding: 0;
        margin: 0;
        background: none;
        box-shadow: none;
        color: red;
    }
    .k-textbox .k-input {
        width: 100%;
    }
    
    .k-tooltip-validation .k-warning {
        display: none;
    }
</style>


</body>
</html>