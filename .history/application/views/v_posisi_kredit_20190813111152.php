<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.material.mobile.min.css" />

    <script src="<?php echo base_url(); ?>assets/telerik-php2/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/telerik-php2/js/kendo.all.min.js"></script>
    

</head>
<body>

<div id="example">
    <div id="grid"></div>
    <script>
        $(document).ready(function () {
            $("#grid").kendoGrid({
                dataSource: {
                    type: "odata",
                    transport: {
                        read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Customers"
                    },
                    pageSize: 20
                },
                height: 550,
                // groupable: true,
                // sortable: true,
                // pageable: {
                //     refresh: true,
                //     pageSizes: true,
                //     buttonCount: 5
                // },
                columns: [{
                    field: "Tanggal",
                    title: "Tanggal Penarikan",
                    width: 240
                }, {
                    field: "Bunga",
                    title: "Bunga dibayar tanggal 23"
                }, {
                    field: "CompanyName",
                    title: "Company Name"
                }, {
                    field: "Country",
                    width: 150
                }]
            });
        });
    </script>
</div>

<style type="text/css">

</style>


</body>
</html>