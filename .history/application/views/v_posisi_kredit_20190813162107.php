<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.material.mobile.min.css" />

    <script src="<?php echo base_url(); ?>assets/telerik-php2/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/telerik-php2/js/kendo.all.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
        

</head>
<body>
    <div class="container-fluid">
        <div class="row" style="border-bottom: 1px solid #EEEEEE">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <h1>Posisi Kredit</h1>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="padding-top: 25px;">
                <input id="products" style="width: 100%" />
            </div>  
        </div>
        <div class="row" style="padding-top: 20px">
            <div class="" style="padding:10px; margin: 0px">
                <div id="grid"></div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var addr = "<?php echo base_url(); ?>index.php/";

            //Dropdown ---------------------------------------------------------------------------------------------//
            $("#products").kendoDropDownList({
                dataTextField: "KET",
                dataValueField: "KODE",
                dataSource: {
                    transport: {
                        read: {
                            dataType: "json",
                            url: addr + "Posisi_kredit/getKodeBank",
                        }
                    }
                }
            });
            //End ofDropdown ---------------------------------------------------------------------------------------//

            //Grid -------------------------------------------------------------------------------------------------//
            $("#grid").kendoGrid({
                dataSource: {
                    type: "odata",
                    transport: {
                        read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Customers"
                    },
                    schema: {
                        model: {
                            fields: {
                                NO: { type: "number" },
                                KD_BANK: { type: "string" },
                                SUPPLIER: { type: "string" },
                                RUPIAH: { type: "date" },
                                TGL_JATUH_TEMPO: { type: "string" },
                                KET: { type: "string" },

                                "NO":"1",
      "KD_BANK":"MDR",
      "SUPPLIER":"KMK TRANSAKSIONAL II",
      "RUPIAH":"250000000000",
      "TGL_JATUH_TEMPO":null,
      "KET":null,
      "CREATED_BY":null,
      "CREATED_DATE":"13-AUG-19 04.31.22.427780 PM",
      "LAST_UPDATE_BY":null,
      "LAST_UPDATE_DATE":null,
      "BUNGA":"9,25",
      "TANGGAL_PENARIKAN":"28\/09\/2018"
                            }
                        }
                    },
                    pageSize: 20
                },
                height: 550,
                groupable: true,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [{
                    field: "ContactName",
                    title: "No",
                    width: 40
                }, {
                    field: "ContactTitle",
                    title: "Bank"
                }, {
                    field: "CompanyName",
                    title: "Jenis"
                }, {
                    field: "Country",
                    title: "Rupiah",
                }, {
                    field: "Country",
                    title: "Bunga Dibayar tanggal 23",
                }, {
                    field: "Country",
                    title: "Tanggal Penarikan",
                }, {
                    field: "Country",
                    title: "Tanggal Jatuh Tempo",
                }, {
                    field: "Country",
                    title: "Keterangan",
                }]
            });
            //End of Grid ------------------------------------------------------------------------------------------//
        });
    </script>
</div>

<style type="text/css">
    body {
        font-family: 'Roboto';
    }
</style>


</body>
</html>