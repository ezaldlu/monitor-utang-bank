<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="styles/kendo.common.min.css" />
    <link rel="stylesheet" href="styles/kendo.default.min.css" />
    <link rel="stylesheet" href="styles/kendo.default.mobile.min.css" />

    <script src="js/jquery.min.js"></script>
    <script src="js/kendo.all.min.js"></script>
    <script src="js/kendo.timezones.min.js"></script>
    

</head>
<body>
<div id="example">
    <div id="team-schedule">
        <div id="people">
            <input checked type="checkbox" id="alex" aria-label="Alex" value="1">
            <input checked type="checkbox" id="bob" aria-label="Bob" value="2">
            <input type="checkbox" id="charlie" aria-label="Charlie" value="3">
        </div>
    </div>
    <div id="scheduler"></div>
</div>
</body>
</html>