<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.material.mobile.min.css" />

    <script src="<?php echo base_url(); ?>assets/telerik-php2/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/telerik-php2/js/kendo.all.min.js"></script>
    

</head>
<body>
    <div class="container">
        <div class="row" style="border-bottom: 1px solid #EEEEEE">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <h1>Posisi Kredit</h1>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="width:30%">
                <input id="products" style="width: 100%" />
            </div>  
        </div>
        <div class="row" style="padding-top: 20px">
            <div id="grid"></div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $("#products").kendoDropDownList({
                dataTextField: "ProductName",
                dataValueField: "ProductID",
                dataSource: {
                    transport: {
                        read: {
                            dataType: "jsonp",
                            url: "https://demos.telerik.com/kendo-ui/service/Products",
                        }
                    }
                }
            });

            $("#grid").kendoGrid({
                dataSource: {
                    type: "odata",
                    transport: {
                        read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Customers"
                    },
                    pageSize: 20
                },
                height: 550,
                groupable: true,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [{
                    field: "ContactName",
                    title: "Contact Name",
                    width: 240
                }, {
                    field: "ContactTitle",
                    title: "Contact Title"
                }, {
                    field: "CompanyName",
                    title: "Company Name"
                }, {
                    field: "Country",
                    width: 150
                }]
            });
        });
    </script>
</div>

<style type="text/css">
    body, @font-face {
        font-family: 'DIN_Medium'; 
        src: url('../fonts/DIN_Medium.eot'); /* IE9 Compatibility Modes */
        src: url('../fonts/DIN_Medium.eot?') format('eot'),  /* IE6-IE8 */
        url('../fonts/DIN_Medium.otf') format('otf'), 
        url('../fonts/DIN_Medium.ttf') format('truetype'); /* Safari, Android, iOS */
    }
</style>


</body>
</html>