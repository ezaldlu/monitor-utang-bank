<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.material.mobile.min.css" />

    <script src="<?php echo base_url(); ?>assets/telerik-php2/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/telerik-php2/js/kendo.all.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
        

</head>
<body>
    <div class="container-fluid">
        <div class="row" style="border-bottom: 1px solid #EEEEEE">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <h1>Posisi Kredit</h1>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="padding-top: 25px;">
                <input id="BankList" style="width: 100%" />
            </div>  
        </div>
        <div class="row" style="padding-top: 20px">
            <div class="" style="padding:10px; margin: 0px">
                <div id="grid"></div>
            </div>
        </div>
        <div id="window" style="display: none;">
            <div id="gridRincian"></div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var addr = "<?php echo base_url(); ?>index.php/";

            //Dropdown ---------------------------------------------------------------------------------------------//
            function onSelect(e) {
                if (e.dataItem) {
                    var dataItem = e.dataItem;
                    // console.log("event :: select (" + dataItem.KET + " : " + dataItem.KODE + ")");
                    $("#grid").data("kendoGrid").dataSource.read({data: dataItem.KODE}); 
                }
            };

            $("#BankList").kendoDropDownList({
                dataTextField: "KET",
                dataValueField: "KODE",
                filter: "startswith",
                select: onSelect,
                dataSource: {
                    transport: {
                        read: {
                            dataType: "json",
                            url: addr + "Posisi_kredit/getKodeBank",
                        }
                    }
                }
            });
            //End ofDropdown ---------------------------------------------------------------------------------------//

            //Grid -------------------------------------------------------------------------------------------------//
            function onChange(arg) {
                var selected = $.map(this.select(), function(item) {
                    return $(item).text();
                });

                console.log("Selected: " + selected.length + " item(s), [" + selected.join(", ") + "]");
            }
            $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: addr + "Posisi_kredit/getAllHutang",
                            dataType: "json",
                            type: "post"
                        }
                    },
                    schema: {
                        model: {
                            id: "NO",
                            fields: {
                                NO: {editable: false},
                                KD_BANK: { field: "KD_BANK" },
                                SUPPLIER: { field: "SUPPLIER" },
                                RUPIAH: { field: "RUPIAH" },
                                TGL_JATUH_TEMPO: { field: "TGL_JATUH_TEMPO" },
                                KET: { field: "KET" },
                                BUNGA: { field: "BUNGA" },
                                TANGGAL_PENARIKAN: { field: "TANGGAL_PENARIKAN" },
                                CREATED_BY: { field: "CREATED_BY" },
                                CREATED_DATE: { field: "CREATED_DATE" },
                                LAST_UPDATE_BY: { field: "LAST_UPDATE_BY" },
                                LAST_UPDATE_DATE: { field: "LAST_UPDATE_DATE"}
                            }
                        }
                    },
                    pageSize: 20
                },
                selectable: "multiple",
                change: onChange,
                toolbar: ["create"],
                editable: "popup",
                height: 550,
                groupable: true,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [{
                    field: "NO",
                    title: "No",
                    width: 40,
                }, 
                {
                    field: "KD_BANK",
                    title: "Kode Bank",
                }, 
                {
                    field: "SUPPLIER",
                    title: "Supplier",
                }, {
                    field: "RUPIAH",
                    title: "Rupiah",
                }, {
                    field: "TGL_JATUH_TEMPO",
                    title: "Tanggal Jatuh Tempo",
                }, {
                    field: "KET",
                    title: "KET",
                }, {
                    template: "#: BUNGA #%",
                    field: "BUNGA",
                    title: "Bunga",
                }, {
                    field: "TANGGAL_PENARIKAN",
                    title: "Tanggal Penarikan",
                }, { command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }]
            });
            $("#gridRincian").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: addr + "Posisi_kredit/getAllHutang",
                            dataType: "json",
                            type: "post"
                        }
                    },
                    schema: {
                        model: {
                            id: "NO",
                            fields: {
                                KD_BANK: { field: "KD_BANK" },
                                SUPPLIER: { field: "SUPPLIER" },
                                RUPIAH: { field: "RUPIAH" },
                                TGL_JATUH_TEMPO: { field: "TGL_JATUH_TEMPO" },
                                KET: { field: "KET" },
                                BUNGA: { field: "BUNGA" },
                                TANGGAL_PENARIKAN: { field: "TANGGAL_PENARIKAN" },
                                CREATED_BY: { field: "CREATED_BY" },
                                CREATED_DATE: { field: "CREATED_DATE" },
                                LAST_UPDATE_BY: { field: "LAST_UPDATE_BY" },
                                LAST_UPDATE_DATE: { field: "LAST_UPDATE_DATE"}
                            }
                        }
                    },
                    pageSize: 20
                },
                selectable: "multiple",
                change: onChange,
                height: 550,
                groupable: true,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [{
                    field: "NO",
                    title: "No",
                    width: 40,
                }, {
                    field: "KD_BANK",
                    title: "Kode Bank",
                }, {
                    field: "SUPPLIER",
                    title: "Supplier",
                }, {
                    field: "RUPIAH",
                    title: "Rupiah",
                }, {
                    field: "TGL_JATUH_TEMPO",
                    title: "Tanggal Jatuh Tempo",
                }, {
                    field: "KET",
                    title: "KET",
                }, {
                    template: "#: BUNGA #%",
                    field: "BUNGA",
                    title: "Bunga",
                }, {
                    field: "TANGGAL_PENARIKAN",
                    title: "Tanggal Penarikan",
                }]
            });
            //End of Grid ------------------------------------------------------------------------------------------//

            //Popup ------------------------------------------------------------------------------------------------//
            var myWindow = $("#window"),
                undo = $("#undo");

            undo.click(function() {
                myWindow.data("kendoWindow").open();
                undo.fadeOut();
            });

            function onClose() {
                undo.fadeIn();
            }

            myWindow.kendoWindow({
                width: "600px",
                title: "Rincian",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            }).data("kendoWindow").center();

            //End of Popup -----------------------------------------------------------------------------------------//
        });
    </script>
</div>

<style type="text/css">
    body {
        font-family: 'Roboto';
    }
</style>


</body>
</html>