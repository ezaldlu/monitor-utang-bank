<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/telerik-php2/styles/kendo.material.mobile.min.css" />

    <script src="<?php echo base_url(); ?>assets/telerik-php2/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/telerik-php2/js/kendo.all.min.js"></script>
    

</head>
<body>

<div id="example">
    <div id="treelist"></div>
    <script>
        $(document).ready(function() {
            var service = "https://demos.telerik.com/kendo-ui/service";

            $("#treelist").kendoTreeList({
                dataSource: {
                    transport: {
                        read: {
                            url: service + "/EmployeeDirectory/All",
                            dataType: "jsonp"
                        }
                    },
                    schema: {
                        model: {
                            id: "EmployeeID",
                            parentId: "ReportsTo",
                            fields: {
                                ReportsTo: { field: "ReportsTo",  nullable: true },
                                EmployeeID: { field: "EmployeeId", type: "number" },
                                Extension: { field: "Extension", type: "number" }
                            },
                            expanded: true
                        }
                    }
                },
                height: 540,
                filterable: true,
                sortable: true,
                columns: [
                    { field: "FirstName", title: "First Name", width: 280,
                        template: $("#photo-template").html() },
                    { field: "LastName", title: "Last Name", width: 160 },
                    { field: "Position" },
                    { field: "Phone", width: 200 },
                    { field: "Extension", width: 140 },
                    { field: "Address" }
                ],
                pageable: {
                    pageSize: 15,
                    pageSizes: true
                }
            });
        });
    </script>
</div>

<style type="text/css">

</style>


</body>
</html>