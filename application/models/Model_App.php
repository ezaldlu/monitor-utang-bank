<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Model_App extends CI_Model
{
    public function getHutang($data)
    {
        $query = $this->db->query("SELECT * FROM TB_HUTANG WHERE KD_BANK = '".$data."'")->result();
        if ($query) {
            # code...
            return $query;
          } else {
            return false;
          }
    }

    public function getSelectedBank($data)
    {
        $query = $this->db->query("SELECT * FROM TB_HUTANG_RINCIAN
                                  LEFT JOIN TB_HUTANG ON TB_HUTANG.NO = TB_HUTANG_RINCIAN.NO
                                  WHERE TB_HUTANG_RINCIAN.NO = ".$data."")->result();
        if ($query) {
          # code...
          return $query;
        } else {
          return false;
        }
        
    }
    
    public function getKodeBank()
    {
        $query = $this->db->query("SELECT KD_BANK AS KODE, KET AS KET FROM TB_BANK")->result();

        if ($query) {
            # code...
            return $query;
          } else {
            return false;
          }
  
    }

    public function addHutang($kode_bank, $supplier, $rupiah, $tgl_jatuh_tempo, $ket, $bunga, $tgl_penarikan)
    {
        $query = $this->db->query("INSERT INTO TB_HUTANG 
                                                          (
                                                                    KD_BANK, 
                                                                    SUPPLIER, 
                                                                      RUPIAH, 
                                                            TGL_JATUH_TEMPO, 
                                                                        KET, 
                                                                      BUNGA, 
                                                            TANGGAL_PENARIKAN
                                                        ) 
                                                  VALUES(
                                                        '".$kode_bank."',
                                                        '".$supplier."',
                                                        ".$rupiah.",
                                                        '".$tgl_jatuh_tempo."',
                                                        '".$ket."',
                                                        '".$bunga."',
                                                        '".$tgl_penarikan."')");

        if ($query) {
          return true;
        } else {
          return false;
        }
    }

    public function deleteHutang($no)
    {
        $this->db->where('NO', $no);
        $query = $this->db->delete('TB_HUTANG');
        
        if($query)
            return true;
        else
            return false;

    }
    
    public function addHutangRincian($no, $bunga, $tgl_jatuh_tempo, $tgl_penarikan, $ket){
        $this->db->query("INSERT INTO TB_HUTANG_RINCIAN 
        (NO,BUNGA,TGL_JATUH_TEMPO,TANGGAL_PENARIKAN,KET)
        VALUES(
          '".$no."',
          '".$bunga."',
          '".$tgl_jatuh_tempo."',
          '".$tgl_penarikan."',
          '".$ket."'
        )");
      }
      public function getHutangRincian(){
     $query=$this->db->query("SELECT * FROM TB_HUTANG_RINCIAN")->result();
        return $query;
      }
      public function getLastRincian(){
       $query= $this->db->query("SELECT *
        FROM ( select a.*, max(TANGGAL_PENARIKAN) over () as max_pk
                 from TB_HUTANG_RINCIAN a
                      )
       where TANGGAL_PENARIKAN = max_pk")->result();
    return $query;  
    }
    public function getLastRincianAndHeader(){
      $query=$this->db->query("SELECT * FROM(SELECT *
      FROM ( select a.*, max(TANGGAL_PENARIKAN) over () as max_pk
               from TB_HUTANG_RINCIAN a
                WHERE No=1
                    )
     where TANGGAL_PENARIKAN = max_pk)  a
     LEFT JOIN TB_HUTANG b ON a.NO=b.NO;")->result();
    return $query;  
  }
}
